<nav id="nekoHeader"class="navbar navbar-expand-lg navbar-dark fixed-top neko-header">
  <div class="container">    
    <a href="#" class="navbar-brand text-light">Beranda</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" href="#about">Tentang</a>
        </li>
        <li class="nav-item ml-2">
          <a class="nav-link" href="#skill">Keahlian</a>
        </li>
        <li class="nav-item ml-2">
          <a class="nav-link" href="#gallery">Galeri</a>
        </li>
        <li class="nav-item ml-2">
          <a class="nav-link" href="#history">Riwayat</a>
        </li>
        <li class="nav-item ml-2">
          <a class="nav-link" href="#download">Unduh CV</a>
        </li>
      </ul>
    </div>
  </div>
</nav>