<script type="text/javascript" src="<?php echo base_url('assets/jquery/js/jquery-3.3.1.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/stellar/js/jquery.stellar.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap4.min.js');?>"></script>
<script type="text/javascript">
    $(window).scroll(function (event) {
        var height = $(window).scrollTop();
        if(height > 100) {
            $("#nekoHeader").addClass("bg-dark");
        } else {            
            $("#nekoHeader").removeClass("bg-dark");
        }
    });
    $(window).stellar();
</script>