<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">        
        <title><?php echo SITE_NAME .": ". ucfirst($this->uri->segment(1)) ." - ". ucfirst($this->uri->segment(2)) ?></title>
        <?php $this->load->view("_php/css.php") ?>
    </head>
    <body>
    <?php $this->load->view("_partials/header.php") ?>
        <div class="introHead">
            <div class="container mt-5 text-white">
                <div class="nekoBio">
                    <div class="nekoPhoto">
                        <div>
                            <img src="<?php echo base_url('assets/opikarief/profile.jpg');?>" alt="">
                        </div>
                    </div>
                    <div class="nekoInfo mt-auto mb-auto">                  
                        <h1>Taufik Arif</h1>
                        <p style="margin-left:7px;"><i><?php echo Date('Y')-1998 ?> Tahun // Bogor // Mahasiswa</i></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="aboutHead" id="about">
            <div class="container text-white">
                <p style="margin:0;">Data Diri</p>
                <h1>& Informasi Saya</h1>
                <div class="m-3">
                    <div class="card-columns">
                        <div class="card bg-transparent">                            
                            <div class="mt-4">NAMA LENGKAP</div>
                            <div class="p-2 d-flex">
                                <div class="nekoBulleting nekoBgColor mr-2"></div>
                                <div class="nekoMultipleLine">
                                    <p>TAUFIK ARIF</p>
                                </div>
                            </div>                           
                            <div class="mt-4">KOTA KELAHIRAN</div>
                            <div class="p-2 d-flex">
                                <div class="nekoBulleting nekoBgColor mr-2"></div>
                                <div class="nekoMultipleLine">
                                    <p>PONTIANAK</p>
                                </div>
                            </div>                         
                            <div class="mt-4">TANGGAL KELAHIRAN</div>
                            <div class="p-2 d-flex">
                                <div class="nekoBulleting nekoBgColor mr-2"></div>
                                <div class="nekoMultipleLine">
                                    <p>25 MEI 1998</p>
                                </div>
                            </div>
                        </div>
                        <div class="card bg-transparent">                            
                            <div class="mt-4">KOTA</div>
                            <div class="p-2 d-flex">
                                <div class="nekoBulleting nekoBgColor mr-2"></div>
                                <div class="nekoMultipleLine">
                                    <p>CIWARINGIN, BOGOR</p>
                                </div>
                            </div>                           
                            <div class="mt-4">ALAMAT</div>
                            <div class="p-2 d-flex">
                                <div class="nekoBulleting nekoBgColor mr-2"></div>
                                <div class="nekoMultipleLine">
                                    <p>JL. CIWARINGIN, GG. SUKARNA, RT04/03 NO. 7</p>
                                </div>
                            </div>                           
                            <div class="mt-4">JENIS KELAMIN</div>
                            <div class="p-2 d-flex">
                                <div class="nekoBulleting nekoBgColor mr-2"></div>
                                <div class="nekoMultipleLine">
                                    <p>LAKI - LAKI</p>
                                </div>
                            </div>
                        </div>
                        <div class="card bg-transparent">                            
                            <div class="mt-4">EMAIL</div>
                            <div class="p-2 d-flex">
                                <div class="nekoBulleting nekoBgColor mr-2"></div>
                                <div class="nekoMultipleLine">
                                    <p>opikarief.id@gmail.com</p>
                                </div>
                            </div>                           
                            <div class="mt-4">TELEPON</div>
                            <div class="p-2 d-flex">
                                <div class="nekoBulleting nekoBgColor mr-2"></div>
                                <div class="nekoMultipleLine">
                                    <p>+62 8963 4670 656</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="skillHead" id="skill">
            <div class="container mt-5 text-white">
                <p style="margin:0;">Keahlian saya</p>
                <h1>Dalam bidang Software</h1>
                <div class="mt-4">
                    <div class="row" style="display: flex;line-height: 4px;">
                        <div class="col">
                            <p><i>" Web Programming "</i></p>
                        </div>
                    </div>
                    <div class="card-columns">
                        <div class="card bg-transparent">
                            <div class="d-flex justify-content-between">
                                <div>PHP</div>
                                <div class="text-right">90%</div>
                            </div>
                            <div class="nekoProgress">
                              <div class="bar" style="width:90%"></div>
                            </div>
                        </div>
                        <div class="card bg-transparent">
                            <div class="d-flex justify-content-between">
                                <div>Code Igniter</div>
                                <div class="text-right">50%</div>
                            </div>
                            <div class="nekoProgress">
                              <div class="bar" style="width:50%"></div>
                            </div>
                        </div>
                        <div class="card bg-transparent">
                            <div class="d-flex justify-content-between">
                                <div>MySQL</div>
                                <div class="text-right">70%</div>
                            </div>
                            <div class="nekoProgress">
                              <div class="bar" style="width:70%"></div>
                            </div>
                        </div>
                        <div class="card bg-transparent">
                            <div class="d-flex justify-content-between">
                                <div>HTML</div>
                                <div class="text-right">80%</div>
                            </div>
                            <div class="nekoProgress">
                              <div class="bar" style="width:80%"></div>
                            </div>
                        </div>
                        <div class="card bg-transparent">
                            <div class="d-flex justify-content-between">
                                <div>CSS</div>
                                <div class="text-right">80%</div>
                            </div>
                            <div class="nekoProgress">
                              <div class="bar" style="width:80%"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-4">
                    <div class="row" style="display: flex;line-height: 4px;">
                        <div class="col">
                            <p><i>" Design "</i></p>
                        </div>
                    </div>
                    <div class="card-columns">
                        <div class="card bg-transparent">
                            <div class="d-flex justify-content-between">
                                <div>Illustrator</div>
                                <div class="text-right">80%</div>
                            </div>
                            <div class="nekoProgress">
                              <div class="bar" style="width:80%"></div>
                            </div>
                        </div>
                        <div class="card bg-transparent">
                            <div class="d-flex justify-content-between">
                                <div>Photoshop</div>
                                <div class="text-right">80%</div>
                            </div>
                            <div class="nekoProgress">
                              <div class="bar" style="width:80%"></div>
                            </div>
                        </div>
                        <div class="card bg-transparent">
                            <div class="d-flex justify-content-between">
                                <div>CorelDraw</div>
                                <div class="text-right">70%</div>
                            </div>
                            <div class="nekoProgress">
                              <div class="bar" style="width:70%"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-4">
                    <div class="row" style="display: flex;line-height: 4px;">
                        <div class="col">
                            <p><i>" Office "</i></p>
                        </div>
                    </div>
                    <div class="card-columns">
                        <div class="card bg-transparent">
                            <div class="d-flex justify-content-between">
                                <div>Word</div>
                                <div class="text-right">80%</div>
                            </div>
                            <div class="nekoProgress">
                              <div class="bar" style="width:80%"></div>
                            </div>
                        </div>
                        <div class="card bg-transparent">
                            <div class="d-flex justify-content-between">
                                <div>Excel</div>
                                <div class="text-right">60%</div>
                            </div>
                            <div class="nekoProgress">
                              <div class="bar" style="width:60%"></div>
                            </div>
                        </div>
                        <div class="card bg-transparent">
                            <div class="d-flex justify-content-between">
                                <div>Power Point</div>
                                <div class="text-right">70%</div>
                            </div>
                            <div class="nekoProgress">
                              <div class="bar" style="width:70%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>      
        <div class="galleryHead" id="gallery">
            <div class="container-fluid text-white">
                <div class="card-columns">
                  <div class="card">
                    <img class="card-img-top" src="<?php echo base_url('assets/opikarief/galeri1.jpg')?>" alt="Card image cap">
                  </div>
                  <div class="card">
                    <img class="card-img-top" src="<?php echo base_url('assets/opikarief/galeri2.jpg')?>" alt="Card image cap">
                  </div>
                  <div class="card">
                    <img class="card-img-top" src="<?php echo base_url('assets/opikarief/galeri3.jpg')?>" alt="Card image cap">
                  </div>
                  <div class="card">
                    <img class="card-img-top" src="<?php echo base_url('assets/opikarief/galeri4.jpg')?>" alt="Card image cap">
                  </div>
                  <div class="card">
                    <img class="card-img-top" src="<?php echo base_url('assets/opikarief/galeri5.jpg')?>" alt="Card image cap">
                  </div>
                  <div class="card">
                    <img class="card-img-top" src="<?php echo base_url('assets/opikarief/galeri6.jpg')?>" alt="Card image cap">
                  </div>
                </div>
            </div>
        </div>
        <div class="educationHead" id="history">
            <div class="container text-white">
                <p style="margin:0;">Riwayat Pendidikan</p>
                <h1>& Pengalaman Kerja</h1>
                <div class="m-3">
                    <div class="card-columns" style="column-count:2;">
                        <div class="card bg-transparent">                            
                            <div class="mt-4">PENDIDIKAN</div>
                            <div class="p-2 d-flex">
                                <div class="nekoBulleting nekoBgColor mr-2"></div>
                                <div class="nekoMultipleLine">
                                    <p>SEKOLAH DASAR // 2005 - 2010</p>
                                    <p>SDN Semplak 1 - Bogor</p>
                                </div>
                            </div>
                            <div class="p-2 d-flex">
                                <div class="nekoBulleting nekoBgColor mr-2"></div>
                                <div class="nekoMultipleLine">
                                    <p>SEKOLAH MENENGAH PERTAMA // 2010 - 2013</p>
                                    <p>SMPN 1 Rancabungur - Ciampea</p>
                                </div>
                            </div>
                            <div class="p-2 d-flex">
                                <div class="nekoBulleting nekoBgColor mr-2"></div>
                                <div class="nekoMultipleLine">
                                    <p>SEKOLAH MENENGAH KEJURUAN // 2013 - 2016</p>
                                    <p>SMK Informatika Bina Generasi - Ciomas</p>
                                </div>
                            </div>
                            <div class="p-2 d-flex">
                                <div class="nekoBulleting nekoBgColor mr-2"></div>
                                <div class="nekoMultipleLine">
                                    <p>UNIVERSITAS // 2016 - Sekarang</p>
                                    <p>Gunadarma - Depok</p>
                                </div>
                            </div>
                        </div>
                        <div class="card bg-transparent">                            
                            <div class="mt-4">PENGALAMAN KERJA</div>
                            <div class="p-2 d-flex">
                                <div class="nekoBulleting nekoBgColor mr-2"></div>
                                <div class="nekoMultipleLine">
                                    <p>KOMINFO KOTA BOGOR // 2015 - Selama 3 Bulan</p>
                                    <p>Praktik Kerja Lapangan (PKL)</p>
                                </div>
                            </div>
                            <div class="p-2 d-flex">
                                <div class="nekoBulleting nekoBgColor mr-2"></div>
                                <div class="nekoMultipleLine">
                                    <p>ESKUL DESIGN // 2013 - Selama SMK</p>
                                    <p>Belajar Mengenai Design</p>
                                </div>
                            </div>
                            <div class="p-2 d-flex">
                                <div class="nekoBulleting nekoBgColor mr-2"></div>
                                <div class="nekoMultipleLine">
                                    <p>PROJECT NUSAHULAWANO // Des 2018</p>
                                    <p>Pembuatan Website Nusa Laut - Backend Dev.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cvHead" align="center" id="download">
            <div class="container text-white">                
                <div class="nekoInfo" style="margin:0;">    
                    <h1>CV Taufik Arif</h1>
                </div>
                <div class="m-3">
                    <a href="<?php echo base_url('/assets/opikarief/asset-cv.pdf');?>" download><button class="btn btn-light p-4" style="border-radius:100px;">Unduh Sekarang</button></a>
                </div>
            </div>
        </div>
        <div class="footerHead">
            <div class="container">
                <span>Copyright © <?php echo SITE_NAME ." ". Date('Y') ?></span>           
            </div>
        </div>  
        <?php $this->load->view("_php/js.php") ?>
    </body>
</html>